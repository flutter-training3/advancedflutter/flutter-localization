import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart'; // แสดงภาษาต่าง ๆ

class DemoLocalization extends StatefulWidget {
  const DemoLocalization({Key? key}) : super(key: key);

  @override
  State<DemoLocalization> createState() => _DemoLocalizationState();
}

class _DemoLocalizationState extends State<DemoLocalization> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Locaization'),
      ),
      body: Center(
        child: Text(AppLocalizations.of(context)!.hello),
      ),
    );
  }
}
