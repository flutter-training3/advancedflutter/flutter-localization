import 'package:flutter/material.dart';
import 'package:flutter_localization/models/app_language.dart';
import 'package:provider/provider.dart';

import 'providers/app_locale.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart'; // แสดงภาษาต่าง ๆ

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // * สำหรับการเปลี่ยนภาษา
  // สร้าง Object โหลด Model สำหรับ Drop down list
  late AppLanguage dropDownValue;
  late String currentDefaultsSystemLocle; // * เก็บภาษาจากระบบ
  int selectLangugeIndex = 0;
  late AppLocale _appLocale;

  @override
  void initState() {
    super.initState();
    dropDownValue = AppLanguage.languges().first;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _appLocale = Provider.of<AppLocale>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.title),
        actions: [
          Padding(
            padding: _appLocale.locale.toString() == 'en'
                ? const EdgeInsets.only(top: 20, right: 10)
                : const EdgeInsets.only(top: 18, right: 10),
            child: Text(AppLocalizations.of(context)!.changelang),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: DropdownButton(
              dropdownColor: Colors.orange,
              iconEnabledColor: Colors.black,
              underline: Container(
                height: 0,
              ),
              onChanged: (AppLanguage? language) {
                dropDownValue = language!;
                _appLocale.changeLocale(Locale(language.languageCode));
              },
              value: dropDownValue,
              items: AppLanguage.languges()
                  .map<DropdownMenuItem<AppLanguage>>((e) => DropdownMenuItem(
                        value: e,
                        child: Text(e.name),
                      ))
                  .toList(),
              // items: const [
              //   DropdownMenuItem(
              //     child: Text(
              //       'English',
              //     ),
              //     value: 'en',
              //   ),
              //   DropdownMenuItem(
              //     child: Text('ไทย'),
              //     value: 'th',
              //   )
              // ],
            ),
          )
        ],
      ),
      body: Center(
        child: Text(AppLocalizations.of(context)!.hello),
      ),
    );
  }
}
