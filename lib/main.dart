import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart'; // แสดงภาษาต่าง ๆ

import 'package:flutter_localization/home_screen.dart';
import 'package:flutter_localization/providers/app_locale.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: AppLocale())],
      child: Consumer<AppLocale>(
        builder: (context, locale, child) => MaterialApp(
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          locale: locale.locale,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(primarySwatch: Colors.orange),
          home: const HomeScreen(),
        ),
      ),
    );
  }
}
