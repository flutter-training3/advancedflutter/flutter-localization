class AppLanguage {
  final String name;
  final String languageCode;

  AppLanguage(this.name, this.languageCode);

  static List<AppLanguage> languges() {
    return <AppLanguage>[
      AppLanguage('English', 'en'),
      AppLanguage('ไทย', 'th'),
    ];
  }

  @override
  bool operator ==(dynamic other) =>
      other != null && other is AppLanguage && name == other.name;

  @override
  // TODO: implement hashCode
  int get hashCode => super.hashCode;




}
