import 'package:flutter/material.dart';

class AppLocale with ChangeNotifier {
  Locale _locale = const Locale('en');
  Locale get locale => _locale;

  void changeLocale(Locale newLocale) {
    _locale = newLocale;
    notifyListeners();
  }
}
